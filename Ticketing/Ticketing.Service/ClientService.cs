﻿using System;
using System.Collections.Generic;
using System.Text;
using Ticketing.Data.Repositories;
using Ticketing.Entities;
using static Ticketing.Service.ClientService;

namespace Ticketing.Service
{
    public class ClientService : IClientService
    {
        private readonly IClientRepository _clientRepository;

        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }
        public Client GetById(int id)
        {
            return _clientRepository.GetById(id);
        }
        public int GetClientByUiid(string uuid)
        {
            return _clientRepository.GetClientByUuid(uuid);
        }
        public int Insert(Client client)
        {
            int exist = _clientRepository.GetClientByUuid(client.Uuid);
            if (exist == 0)
            {
                _clientRepository.Insert(client);
            }
            _clientRepository.SaveChanges();
            return client.Id;
        }
        public int Update(Client oldclient, Client client)
        {
            int exist = _clientRepository.GetClientByUuid(client.Uuid);
            if (exist == 0)
            {
                oldclient.Uuid = client.Uuid;
                int rows = _clientRepository.SaveChanges();
                return client.Id;

            }
            return 0;

        }
        public int Delete(Client client)
        {
            _clientRepository.Remove(client);
            int rows = _clientRepository.SaveChanges();
            return rows;

        }

        public interface IClientService
        {
            Client GetById(int id);
            int Insert(Client client);
            int Update(Client oldclient, Client client);
            int Delete(Client client);
            int GetClientByUiid(string uuid);
        }
    }
}
