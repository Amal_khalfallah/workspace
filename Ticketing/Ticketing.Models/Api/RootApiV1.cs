﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Models.Api
{
    public class AgencyApi
    {
        public string uuid { get; set; }
        public string name { get; set; }
    }

    public class OfficeApi
    {
        public string office_id { get; set; }
        public string name { get; set; }
        public AgencyApi agency { get; set; }
    }

    public class AgentBookingApi
    {
        public string uuid { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string sine { get; set; }
        public OfficeApi office { get; set; }
    }
    public class AgentTicketingApi
    {
        public string uuid { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string sine { get; set; }
        public OfficeApi office { get; set; }
    }
    


    public class PassengerApi
    {
        public string name { get; set; }
    }

    public class RelatedToApi
    {
        public string tktnbr { get; set; }
        public string status { get; set; }
    }

    public class CurrencyApi
    {
        public string iso3 { get; set; }
    }

    public class ClientApi
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public string account_number { get; set; }
    }

    public class RootApiV1
    {
        public List<string> rizs { get; set; }
        public string route { get; set; }
        public string tktnbr { get; set; }
        public AgentBookingApi agent_booking { get; set; }
        public AgentTicketingApi agent_ticketing { get; set; }
        public string emd { get; set; }
        public string from_epower { get; set; }
        public string has_related_ticket { get; set; }
        public string date_change { get; set; }
        public string date_refund_change { get; set; }
        public PassengerApi passenger { get; set; }
        public string date_status { get; set; }
        public string pnr { get; set; }
        public string total_price { get; set; }
        public string price { get; set; }
        public RelatedToApi related_to { get; set; }
        public CurrencyApi currency { get; set; }
        public string status { get; set; }
        public ClientApi client { get; set; }
        public string service_fee { get; set; }
        public string airline_commision { get; set; }
        public string airline_commision_refund { get; set; }
        public string refund { get; set; }
    }

    public class PaginationApiV1
    {
        public string per_page { get; set; }
        public string current { get; set; }
        public string max_index { get; set; }
        public string total { get; set; }
    }

    public class RootObjectApiV1
    {
        public List<RootApiV1> result { get; set; }
        public PaginationApiV1 pagination { get; set; }
    }
}
