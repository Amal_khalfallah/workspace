﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using Ticketing.Entities;
using static Ticketing.Service.ClientService;

namespace formqtion.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClientController : ControllerBase
    {
        private readonly IClientService clientService;

        public ClientController(IClientService clientService)
        {
            this.clientService = clientService;
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var client = new Client();
            try
            {
                client = clientService.GetById(id);
                if (client == null)
                {
                    return NotFound();
                }
            }
            catch (Exception e)
            {
                Response.StatusCode = 500;
            }
            return Ok(client);
        }

       /* [HttpGet("getByName")]
        public ActionResult Get(string name)
        {
            var clients = clientService.GetByName(name);
            return Ok(clients);
        }*/
        [HttpPost]
        public ActionResult Post([FromBody]Client client)
        {
            try
            {
                var clientId = clientService.Insert(client);
                return Ok(client);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
        [HttpPut]
        public ActionResult Put([FromBody]Client client)
        {
            try
            {
                var oldclient = clientService.GetById(client.Id);
                if (oldclient == null)
                {
                    return NotFound();
                }
                clientService.Update(oldclient, client);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            return NoContent();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                var client = clientService.GetById(id);
                if (client == null)
                {
                    return NotFound();
                }
                clientService.Delete(client);
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
            return NoContent();
        }

    }
}
