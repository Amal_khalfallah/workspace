﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Data.Utils
{
    public class ListUtils
    {
        public static string GetMonth(int month)
        {
            string result = "";

            switch (month)
            {
                case 1:
                    result = "JAN";
                    break;
                case 2:
                    result = "FÉV";
                    break;
                case 3:
                    result = "MAR";
                    break;
                case 4:
                    result = "AVR";
                    break;
                case 5:
                    result = "MAI";
                    break;
                case 6:
                    result = "JUN";
                    break;
                case 7:
                    result = "JUL";
                    break;
                case 8:
                    result = "AOÛ";
                    break;
                case 9:
                    result = "SEP";
                    break;
                case 10:
                    result = "OCT";
                    break;
                case 11:
                    result = "NOV";
                    break;
                case 12:
                    result = "DÉC";
                    break;
                default:
                    result = "";
                    break;
            }

            return result;
        }
    }
}
