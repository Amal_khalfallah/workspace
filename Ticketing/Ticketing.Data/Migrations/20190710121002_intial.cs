﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Ticketing.Data.Migrations
{
    public partial class intial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "agencies",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Uuid = table.Column<string>(nullable: true),
                    name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_agencies", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Clients",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Uuid = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Adress = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Clients", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "emdRelatedTo",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    tktnbr = table.Column<string>(nullable: true),
                    status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_emdRelatedTo", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Offices",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    agencyId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Offices", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Offices_agencies_agencyId",
                        column: x => x.agencyId,
                        principalTable: "agencies",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "agenciesBooking",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Uuid = table.Column<string>(nullable: true),
                    first_name = table.Column<string>(nullable: true),
                    last_name = table.Column<string>(nullable: true),
                    sine = table.Column<string>(nullable: true),
                    officeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_agenciesBooking", x => x.Id);
                    table.ForeignKey(
                        name: "FK_agenciesBooking_Offices_officeId",
                        column: x => x.officeId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "agentTickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    Uuid = table.Column<string>(nullable: true),
                    first_name = table.Column<string>(nullable: true),
                    last_name = table.Column<string>(nullable: true),
                    sine = table.Column<string>(nullable: true),
                    officeId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_agentTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_agentTickets_Offices_officeId",
                        column: x => x.officeId,
                        principalTable: "Offices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Created = table.Column<DateTime>(nullable: false),
                    Modified = table.Column<DateTime>(nullable: false),
                    rizs = table.Column<string>(nullable: true),
                    route = table.Column<string>(nullable: true),
                    tktnbr = table.Column<string>(nullable: true),
                    agent_bookingId = table.Column<int>(nullable: true),
                    agent_ticketingId = table.Column<int>(nullable: true),
                    emd = table.Column<int>(nullable: false),
                    from_epower = table.Column<bool>(nullable: false),
                    has_related_ticket = table.Column<bool>(nullable: false),
                    date_change = table.Column<string>(nullable: true),
                    date_refund_change = table.Column<string>(nullable: true),
                    PassengerName = table.Column<string>(nullable: true),
                    date_status = table.Column<string>(nullable: true),
                    pnr = table.Column<string>(nullable: true),
                    total_price = table.Column<double>(nullable: false),
                    price = table.Column<double>(nullable: false),
                    related_toId = table.Column<int>(nullable: true),
                    status = table.Column<string>(nullable: true),
                    clientId = table.Column<int>(nullable: true),
                    service_fee = table.Column<double>(nullable: false),
                    airline_commision = table.Column<double>(nullable: false),
                    airline_commision_refund = table.Column<double>(nullable: false),
                    refund = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tickets_agenciesBooking_agent_bookingId",
                        column: x => x.agent_bookingId,
                        principalTable: "agenciesBooking",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tickets_agentTickets_agent_ticketingId",
                        column: x => x.agent_ticketingId,
                        principalTable: "agentTickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tickets_Clients_clientId",
                        column: x => x.clientId,
                        principalTable: "Clients",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tickets_emdRelatedTo_related_toId",
                        column: x => x.related_toId,
                        principalTable: "emdRelatedTo",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_agenciesBooking_officeId",
                table: "agenciesBooking",
                column: "officeId");

            migrationBuilder.CreateIndex(
                name: "IX_agentTickets_officeId",
                table: "agentTickets",
                column: "officeId");

            migrationBuilder.CreateIndex(
                name: "IX_Offices_agencyId",
                table: "Offices",
                column: "agencyId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_agent_bookingId",
                table: "Tickets",
                column: "agent_bookingId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_agent_ticketingId",
                table: "Tickets",
                column: "agent_ticketingId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_clientId",
                table: "Tickets",
                column: "clientId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_related_toId",
                table: "Tickets",
                column: "related_toId");

            migrationBuilder.CreateIndex(
                name: "IX_Tickets_tktnbr",
                table: "Tickets",
                column: "tktnbr");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tickets");

            migrationBuilder.DropTable(
                name: "agenciesBooking");

            migrationBuilder.DropTable(
                name: "agentTickets");

            migrationBuilder.DropTable(
                name: "Clients");

            migrationBuilder.DropTable(
                name: "emdRelatedTo");

            migrationBuilder.DropTable(
                name: "Offices");

            migrationBuilder.DropTable(
                name: "agencies");
        }
    }
}
