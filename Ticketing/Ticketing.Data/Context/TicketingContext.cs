﻿using Ticketing.Entities;
using Ticketing.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ticketing.Data.Extentions;

namespace Ticketing.Data.Context
{
    public class TicketingContext : DbContext
    {
        public TicketingContext(DbContextOptions<TicketingContext> options): base(options)
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Agency> agencies { get; set; }
        public DbSet<AgentBooking> agenciesBooking { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<EmdRelatedTo> emdRelatedTo { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<AgentTicketing> agentTickets { get; set; }
        public DbQuery<ClientVMs> ClientVMs { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // add your own confguration here
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Ticket>()
             .HasIndex(u => u.tktnbr);
            //DOCUMENTAION
        }
        public override int SaveChanges()
        {
            ChangeTracker.ApplyAuditInformation();
            return base.SaveChanges();
        }

    }

}
