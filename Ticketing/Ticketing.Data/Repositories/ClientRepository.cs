﻿using Ticketing.Data.Context;
using Ticketing.Entities;
using Ticketing.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ticketing.Data.Repositories
{
    public class ClientRepository : BaseRepository<Client>, IClientRepository
    {
        public ClientRepository(TicketingContext context) : base(context)
        {
        }

        public List<ClientVMs> GetByName(string name)
        {

            var clients = context.ClientVMs.FromSql(
                    @"select c.Name, c.Adress AS AdressName
                    from  Clients  c 
                    ").ToList();
            return clients;
        }

        public int GetClientByUuid(string uuid)
        {
            var clientId = context.Clients.Where(i => i.Uuid == uuid).Select(i => i.Id).FirstOrDefault();
            if (clientId != 0 && !string.IsNullOrEmpty(uuid))
            {
                return clientId;
            }
            return 0;
        }
    }
    public interface IClientRepository : IRepository<Client>
    {
        List<ClientVMs> GetByName(string name);
        int GetClientByUuid(string uuid);
    }
}
