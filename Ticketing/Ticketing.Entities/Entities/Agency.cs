﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ticketing.Entities
{
   public class Agency : BaseAuditClass
    {
        public string Uuid { get; set; }
        public string name { get; set; }
        public Office office { get; set; }
    }
}
