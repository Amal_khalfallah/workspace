﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Entities
{
    public class Ticket : BaseAuditClass
    {
        public string rizs { get; set; }
        public string route { get; set; }
        public string tktnbr { get; set; }
        public AgentBooking agent_booking { get; set; }
        public AgentTicketing agent_ticketing { get; set; }
        public int emd { get; set; }
        public bool from_epower { get; set; }
        public bool has_related_ticket { get; set; }
        public string date_change { get; set; }
        public string date_refund_change { get; set; }
        public string PassengerName { get; set; }
        public string date_status { get; set; }
        public string pnr { get; set; }
        public double total_price { get; set; }
        public double price { get; set; }
        public EmdRelatedTo related_to { get; set; }
        public string status { get; set; }
        public ICollection<Client> clients { get; set; }
        public double service_fee { get; set; }
        public double airline_commision { get; set; }
        public double airline_commision_refund { get; set; }
        public double refund { get; set; }
    }

}
