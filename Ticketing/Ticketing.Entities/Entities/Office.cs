﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Entities
{
    public class Office : BaseAuditClass
    { 
        public string name { get; set; }
        public ICollection<Agency> agencies { get; set; }
        public ICollection<AgentTicketing> agentTicketings { get; set; }
        public ICollection<AgentBooking> agentBookings { get; set; }
        
    }
}
