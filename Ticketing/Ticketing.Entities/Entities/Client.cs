﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Ticketing.Entities
{

    public class Client : BaseAuditClass
    {
        public Client()
        {

        }
        public string Uuid { get; set; }
        public string Name { get; set; }
        public string Adress { get; set; }
    }

}
