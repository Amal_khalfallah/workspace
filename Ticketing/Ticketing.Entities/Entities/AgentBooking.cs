﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Entities
{
    public class AgentBooking : BaseAuditClass
    {
        public string Uuid { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string sine { get; set; }
        public Office office { get; set; }
        public ICollection<Ticket> tickets { get; set; }
    }
}
