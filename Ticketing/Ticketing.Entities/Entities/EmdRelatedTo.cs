﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Entities
{

    public class EmdRelatedTo : BaseAuditClass
    {
        public string tktnbr { get; set; }
        public string status { get; set; }
        public ICollection<Ticket> tickets { get; set; }
    }
}
