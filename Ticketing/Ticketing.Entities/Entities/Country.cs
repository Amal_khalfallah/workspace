﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Entities.Entities
{
    public class Country
    {
        public int CountryId { get; set; }
        public string OwnId_country { get; set; }
        public string NameAr { get; set; }
        public string NameUs { get; set; }
        public string NameTr { get; set; }
        public string NameCountryUppercase { get; set; }
        public string NameFr { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string CallingCode { get; set; }
        public bool Enabled { get; set; }
    }
}
