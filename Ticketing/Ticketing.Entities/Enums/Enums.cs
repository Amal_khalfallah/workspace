﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Entities.Enums
{
    public class Enums
    {
        public enum Period
        {
            Today = 1,
            Yesterday,
            ThisWeek,
            LastWeek,
            ThisMonth,
            LastMonth,
            ThisYear,
            Customize,
            Day
        }
    }
}
