﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ticketing.Entities.Constants
{
    public static class TicketStatusConstants
    {
        public const string TKTT = "TKTT";
        public const string RFND = "RFND";
        public const string RFNC = "RFNC";
        public const string EMDS = "EMDS";
        public const string EMDA = "EMDA";
        public const string ACMA = "ACMA";
        public const string ACMD = "ACMD";
        public const string ACNT = "ACNT";
        public const string ADMA = "ADMA";
        public const string ADMD = "ADMD";
        public const string ADNT = "ADNT";
        public const string SPCR = "SPCR";
        public const string SPDR = "SPDR";
        public const string SSAC = "SSAC";
        public const string SSAD = "SSAD";
        public const string TASF = "TASF";
        public const string CANX = "CANX";
        public const string CANR = "CANR";
        public const string CANN = "CANN";
        public const string CNJ = "CNJ";
    }
}
